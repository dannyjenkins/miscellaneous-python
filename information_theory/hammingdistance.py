"""Minimum Hamming distance converter.

69b70b8f97fd1377de87e527dc580b7115b07d2bc7f10c9d2247d62d0ac3f8a1

Provide an integer argument to this program, and it will permute all
possible binary codes of n=5 and k=argument, and print out the minimum
Hamming distance for each of them.
"""

import itertools
import sys


def list_of_candidates():
    """Generate numbers 0-32 in binary, padded to 5 characters"""
    return_list = []
    for number in range(32):
        word = bin(number)[2:]
        word = ((5 - len(word)) * '0') + word
        return_list.append(word)
    return return_list


def hamming_distance(word1, word2):
    """Return the Hamming distance between two words."""
    counter = 0
    for character in range(len(word1)):
        if word1[character] != word2[character]:
            counter += 1
    return counter


def minimum_hamming_distance(code):
    """Return the minimum Hamming distance of a code."""
    all_pairs = list(itertools.combinations(code, 2))
    minimum_distance = hamming_distance(all_pairs[0][0], all_pairs[0][1])
    for pair in all_pairs:
        if hamming_distance(pair[0], pair[1]) < minimum_distance:
            minimum_distance = hamming_distance(pair[0], pair[1])
    return minimum_distance


def combinations(list_of_words, dimension):
    """Return all permutations of a list of a given dimension."""
    return_list = list(itertools.combinations(list_of_words, dimension))
    return return_list


for x in combinations(list_of_candidates(), int(sys.argv[1])):
    print(x, minimum_hamming_distance(x))

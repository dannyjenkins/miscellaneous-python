"""Functions for calculating the entropy of a random variable."""

import math
from fractions import Fraction


def valid_values(a_list):
    """Check if the sum of a set of fractions equals 1."""
    total_value = Fraction(0, 1)
    for value in a_list:
        total_value += value
    return total_value == Fraction(1, 1)


def prob_times_log_prob(x):
    """Get the input number times the logarithm, base 2, of itself."""
    if x == 0:
        return 0
    return x * math.log(x, 2.0)


def calculate_entropy(a_list):
    """Calculate the entropy of a list."""
    return_value = 0
    for x in a_list:
        return_value += prob_times_log_prob(x)
    return abs(return_value)

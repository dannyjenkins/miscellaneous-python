"""Functions for creating and operating on matrices."""

from fractions import Fraction


def create_matrix(input_list):
    """Create a matrix from two dimensions and a set of elements.

    The first element of the input list will be the number of columns,
    and the second element will be the number of rows.  The rest of
    the elements in the input list will then be pigeonholed, in order,
    into this matrix.
    """
    x = int(input_list[0])
    y = int(input_list[1])
    input_list = input_list[2:]
    assert len(input_list) == x * y

    list_of_lists = []
    for num in range(y):
        list_of_lists.append([])
    for each_list in list_of_lists:
        for num in range(x):
            each_list.append(Fraction(input_list.pop(0)))
    return list_of_lists
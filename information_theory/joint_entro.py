"""Module for calculating marginal distributions."""

from fractions import Fraction


def sum_rows(matrix):
    """Get the sum of each row in a matrix."""
    return [sum(x) for x in matrix]


def sum_columns(matrix):
    """Get the sum of each column in a matrix."""
    return_list = []
    for x in range(len(matrix[0])):
        return_list.append(sum([the_list[x] for the_list in matrix]))
    return return_list


def marginal_distribution(matrix):
    """Get the marginal distribution of a matrix."""
    return_list = []
    return_list.append(sum_rows(matrix))
    return_list.append(sum_columns(matrix))
    return return_list


def valid_values(matrix):
    """Make sure that the values of a matrix are valid.

    This is done by making sure that the sum of the rows, as well as
    the sum of the columns, both equal 1.
    """
    return (
        sum(sum_rows(matrix)) == Fraction(1, 1) and
        sum(sum_columns(matrix)) == Fraction(1, 1)
    )

"""The main module."""

import sys
from fractions import Fraction
from matrix import create_matrix
from entro import valid_values as random_var_valid_values
from entro import calculate_entropy
from joint_entro import valid_values as joint_valid_values
from joint_entro import sum_rows
from joint_entro import sum_columns

if len(sys.argv) == 1:
    print("Error: Please provide an option argument.")
    exit()

if sys.argv[1] not in ["--single", "--single-no-bound", "--joint", "--binary"]:
    print("Error: Invalid option argument.")
    exit()

if sys.argv[1] == "--single":
    random_variable = [Fraction(x) for x in sys.argv[2:]]
    assert random_var_valid_values(random_variable)
    print(calculate_entropy(random_variable))

if sys.argv[1] == "--single-no-bound":
    random_variable = [Fraction(x) for x in sys.argv[2:]]
    print(calculate_entropy(random_variable))

if sys.argv[1] == "--joint":
    input_list = sys.argv[2:]
    the_matrix = create_matrix(input_list)
    assert joint_valid_values(the_matrix)
    print("The marginal distribution of the rows: "
          + str(sum_rows(the_matrix)))
    print("The marginal distribution of the columns: "
          + str(sum_columns(the_matrix)))

if sys.argv[1] == "--binary":
    the_matrix = create_matrix(input_list)

import csv

keys = []

with open('ferdig_combined.csv', newline='') as csvfile:
    the_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in the_reader:
        keys.append(row)
for x in range(len(keys)):
    keys[x] = keys[x][0]


def xor(list1, list2):
    output = []
    for x in range(len(list1)):
        group4 = ""
        for y in range(len(list1)):
            group4 += '0' if list1[x][y] == list2[x][y] else '1'
        output.append(group4)
    return output


def p_swap(pi_p, input_group, m, l):
    output = []
    count = 0
    for _x in range(m):
        output.append("")
        for _y in range(l):
            grab_index = pi_p[count + 1] - 1
            nth_group = grab_index // m
            nth_in_group = grab_index % l
            output[-1] += input_group[nth_group][nth_in_group]
            count += 1
    return output


def alg(l, m, x, pi_s, pi_p, rk):
    w = []
    v = []
    u = []
    w.append(x)
    nr = len(rk) - 1
    for r in range(0, nr):
        u_r = xor(w[r], rk[r])
        v_r = []
        for i in range(m):
            v_r.append(pi_s[u_r[i]])
        v.append(v_r)
        w.append(p_swap(pi_p, v_r, m, l))
        u.append(u_r)
    u_nr = xor(w[nr - 1], rk[nr - 1])
    v_nr = []
    for i in range(m):
        v_nr.append(pi_s[u_nr[i]])
    y = xor(v_nr, rk[nr])
    return y


pi_s = {
    "0000": "1110",
    "0001": "0100",
    "0010": "1101",
    "0011": "0001",
    "0100": "0010",
    "0101": "1111",
    "0110": "1011",
    "0111": "1000",
    "1000": "0011",
    "1001": "1010",
    "1010": "0110",
    "1011": "1100",
    "1100": "0101",
    "1101": "1001",
    "1110": "0000",
    "1111": "0111"
    }

pi_p = {
    1: 1,
    2: 5,
    3: 9,
    4: 13,
    5: 2,
    6: 6,
    7: 10,
    8: 14,
    9: 3,
    10: 7,
    11: 11,
    12: 15,
    13: 4,
    14: 8,
    15: 12,
    16: 16
    }

for key_index, key_value in enumerate(keys):

    if key_index % 1000 == 0:
        print(key_index)
    
    K = [
        key_value[0:4],
        key_value[4:8],
        key_value[8:12],
        key_value[12:16],
        key_value[16:20],
        key_value[20:24],
        key_value[24:28],
        key_value[28:32],
        ]

    round_keys = [
        K[0:4],
        K[1:5],
        K[2:6],
        K[3:7],
        K[4:8]
        ]

    if \
        alg(4, 4, ["1110","0110","1100","0101"], pi_s, pi_p, round_keys) == ["1100","1110","0110","0111"] and \
        alg(4, 4, ["0001","0001","0000","1010"], pi_s, pi_p, round_keys) == ["1001","1101","1100","0101"] and \
        alg(4, 4, ["1000","0100","1111","0101"], pi_s, pi_p, round_keys) == ["0001","1100","0010","0111"] and \
        alg(4, 4, ["0111","0010","1010","0111"], pi_s, pi_p, round_keys) == ["1001","1111","1010","0000"]:
        print(key_index, key_value)
        break
    

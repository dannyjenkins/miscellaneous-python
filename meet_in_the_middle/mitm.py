import bisect

l = 4
m = 4

input_x1  = ["1110", "0110", "1100", "0101"]
output_y1 = ["1100", "1110", "0110", "0111"]

def dtb(decnum: int, digits_num: int) -> str:
    """Decimal to binary"""
    return bin(decnum)[2:].zfill(digits_num)


def btd(binary: str) -> int:
    """Binary to decimal"""
    return int(binary, 2)


def concat(list1) -> str:
    """Concatenate a list of strings into a single string."""
    output = ""
    for x in list1:
        output += x
    return output


def find_in_sorted_list(elem, sorted_list):
    """Find an element in a sorted list via binary search."""
    i = bisect.bisect_left(sorted_list, elem)
    if i != len(sorted_list) and sorted_list[i] == elem:
        return i
    return -1


def in_sorted_list(elem, sorted_list):
    """Check if an element exists in a sorted list via binary search."""
    i = bisect.bisect_left(sorted_list, elem)
    return i != len(sorted_list) and sorted_list[i] == elem


def xor(list1, list2):
    dec1 = btd(concat(list1))
    dec2 = btd(concat(list2))
    xor_result = dec1 ^ dec2
    result_bin = dtb(xor_result, 16)
    return [result_bin[0:4], result_bin[4:8], result_bin[8:12], result_bin[12:16]]


def p_swap(pi_p, input_group, m, l):
    """Perform swaps, according to pi_p."""
    output = []
    count = 0
    for _x in range(m):
        output.append("")
        for _y in range(l):
            grab_index = pi_p[count + 1] - 1
            nth_group = grab_index // m
            nth_in_group = grab_index % l
            output[-1] += input_group[nth_group][nth_in_group]
            count += 1
    return output


def alg_1_step_first(x, k_n):
    u1 = xor(x, k_n)
    v1 = []
    for i in range(m):
        v1.append(pi_s[u1[i]])
    return v1


def alg_1_step(v_rminus1, k_n):
    w = p_swap(pi_p, v_rminus1, m, l)
    u_r = xor(w, k_n)
    v_r = []
    for i in range(m):
        v_r.append(pi_s[u_r[i]])
    return v_r


def algb_1_step_first(y, k_n):
    v4 = xor(y, k_n)
    return v4


def algb_1_step_second(v4, k4):
    u4 = []
    for i in range(m):
        u4.append(pi_sb[v4[i]])
    w3 = xor(u4, k4)
    v3 = p_swap(pi_p, w3, m, l)
    return v3


def algb_1_step(v_r, k_n):
    u_r = []
    for i in range(m):
        u_r.append(pi_sb[v_r[i]])
    w_r = xor(u_r, k_n)
    v_rminus1 = p_swap(pi_p, w_r, m, l)
    return v_rminus1


bitgroups4 = [
    "0000",
    "0001",
    "0010",
    "0011",
    "0100",
    "0101",
    "0110",
    "0111",
    "1000",
    "1001",
    "1010",
    "1011",
    "1100",
    "1101",
    "1110",
    "1111",
    ]

pi_s = {
    "0000": "1110",
    "0001": "0100",
    "0010": "1101",
    "0011": "0001",
    "0100": "0010",
    "0101": "1111",
    "0110": "1011",
    "0111": "1000",
    "1000": "0011",
    "1001": "1010",
    "1010": "0110",
    "1011": "1100",
    "1100": "0101",
    "1101": "1001",
    "1110": "0000",
    "1111": "0111"
    }

pi_sb = {
    "1110": "0000",
    "0100": "0001",
    "1101": "0010",
    "0001": "0011",
    "0010": "0100",
    "1111": "0101",
    "1011": "0110",
    "1000": "0111",
    "0011": "1000",
    "1010": "1001",
    "0110": "1010",
    "1100": "1011",
    "0101": "1100",
    "1001": "1101",
    "0000": "1110",
    "0111": "1111"
    }

pi_p = {
    1: 1,
    2: 5,
    3: 9,
    4: 13,
    5: 2,
    6: 6,
    7: 10,
    8: 14,
    9: 3,
    10: 7,
    11: 11,
    12: 15,
    13: 4,
    14: 8,
    15: 12,
    16: 16
    }


# All possible values after passing through 3 K-functions, from x
# {Partial K-value from beginning : v^2 value}
K3f = {}

for x0 in bitgroups4:
    for x1 in bitgroups4:
        for x2 in bitgroups4:
            for x3 in bitgroups4:
                for x4 in bitgroups4:
                    for x5 in bitgroups4:
                        concatn3 = concat([x0, x1, x2, x3, x4, x5])
                        concatn3_int = btd(concatn3)

                        second = alg_1_step_first(input_x1, [x0, x1, x2, x3])
                        third = alg_1_step(second, [x1, x2, x3, x4])

                        third_int = [
                            btd(third[0]),
                            btd(third[1]),
                            btd(third[2]),
                            btd(third[3]),
                            ]

                        # 24 bits : [4 4-bit groups]
                        K3f[concatn3_int] = third_int

# All intersections between K1 and K2
K1iK2fstr = []
K1iK2fstrwithK1 = []

# x is concatn3_int
for x in K3f:
    # k_value is the four middle values of K
    k_value = dtb(x, 24)[8:]
    s_value = K3f[x]
    K1iK2fstr.append(f"{btd(k_value)} {s_value}")
    K1iK2fstrwithK1.append(f"{btd(k_value)} {s_value} {x}")

K3f = {}

K1iK2fstr.sort()
K1iK2fstrwithK1.sort()

for x2 in bitgroups4:
    for x3 in bitgroups4:
        for x4 in bitgroups4:
            for x5 in bitgroups4:
                for x6 in bitgroups4:
                    for x7 in bitgroups4:
                        concatn5 = concat([x2,x3,x4,x5,x6,x7])
                        concatn5_int = btd(concatn5)

                        lastminus1 = algb_1_step_first(output_y1, [x4, x5, x6, x7])
                        lastminus2 = algb_1_step_second(lastminus1, [x3, x4, x5, x6])
                        lastminus3 = algb_1_step(lastminus2, [x2, x3, x4, x5])

                        lastminus3_int = [
                            btd(lastminus3[0]),
                            btd(lastminus3[1]),
                            btd(lastminus3[2]),
                            btd(lastminus3[3]),
                            ]

                        k_value = concatn5_int
                        k_value = dtb(k_value, 28)
                        k_value = k_value[:-8]
                        k_value = btd(k_value)
                        s_value = lastminus3_int
                        pair    = (k_value, s_value)
                        pairb   = (dtb(pair[0], 16), dtb(pair[1][0], 4) + dtb(pair[1][1], 4) + dtb(pair[1][2], 4) + dtb(pair[1][3], 4))

                        if in_sorted_list(f"{pair[0]} {pair[1]}", K1iK2fstr):
                    
                            # The index of the pair in the list of overlapping K-values
                            index = find_in_sorted_list(f"{pair[0]} {pair[1]}", K1iK2fstr)
                    
                            # The pair, including the full K1 at the end
                            full = K1iK2fstrwithK1[index]
                    
                            # The K-value except for the last 8 bits (i.e. concatn3), in decimal
                            # form
                            n1 = int(full.split()[5])

                            # The first 8 bits of the K-value
                            beginning = dtb(n1, 24)[:8]
                    
                            # The end of the K-value
                            end = dtb(concatn5_int, 28)[12:]
                            combined = beginning + pairb[0] + end[8:]
                    
                            print(f"end: {end} ", end="")
                            print(f"v^2: {pairb[1]} ", end="")
                            print(f"4 middle + 2 last in K: {pairb[0] + end[8:]} ", end="")
                            print(f"2 first: {beginning} ", end="")
                            print(f"Combined: {combined}")

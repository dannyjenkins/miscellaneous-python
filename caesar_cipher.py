"""Caesar cipher decoder."""

import sys

abc = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k",
            "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v",
            "w", "x", "y", "z"]

for argument in sys.argv:
    if argument.isdigit():
        offset = int(argument)
    else:
        phrase = argument


def caesarCipher(aString, anOffset):
    """
    Return a string, offset by a given number of characters in the
    alphabet, to perform a Caesar cipher.

    :param aString: The string to convert.

    :param anOffset: The offset to use, can be positive or negative.

    :returns: The converted string.
    """
    stringList = []
    for x in aString:
        if x not in [" ", ",", "."]:
            x = x.lower()
            index = abc.index(x)
            stringList.append(abc[(index + anOffset) % len(abc)])
        else:
            stringList.append(x)
    return "".join(stringList)


print(caesarCipher(phrase, offset))

"""
5fa326ca59860a63c683cb32e8ecff9d.

This script takes an integer as argument, and generates all possible
strings of this length using the character set below (or a different
character set, if specified with the option '-c'), along with their
MD5 hashes.

Example 1: given the argument '-p 5', the script will output all
strings from "aaaaa" to "99999", along with the MD5 hash of each
string.

Example 2: given the argument '-c q1w2 -p 6', the script will output
all six-character strings that are possible to construct using the
characters q, 1, w and 2, along with the MD5 hash of each string.
"""

import hashlib
import sys

chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123" \
    "456789"


def getStr(aList):
    """Convert a list of chars into a string.

    :param aList: The list to be converted.

    :returns: The string.

    """
    theString = ""
    for position in aList:
        theString += chars[position]
    return theString


def assignValues(aList, aNumber):
    expon = len(aList) - 1
    for element in range(len(aList)):
        aList[element] = (aNumber // len(chars) ** expon) % len(chars)
        expon -= 1


positions = []

for argument in range(len(sys.argv)):

    if "-p" not in sys.argv:
        print("Syntax error: String length argument missing.\n"
              "Use -p followed by an integer to specify this"
              "argument.")
        exit()

    if sys.argv[argument] == "-p":
        if sys.argv[argument] == sys.argv[-1]:
            print("Syntax error: String length argument incomplete."
                  "\nUse -p followed by an integer to specify this "
                  " argument.")
            exit()
        elif not sys.argv[argument+1].isdigit():
            print("Syntax error: String length argument invalid." \
                  "\nUse -p followed by an integer to specify this " \
                  "argument.")
            exit()
        elif sys.argv[argument+1].isdigit():
            for position in range(int(sys.argv[argument+1])):
                positions.append(0)

    elif sys.argv[argument] == "-c":
        if sys.argv[argument] == sys.argv[-1]:
            print("Syntax error: -c argument missing")
            exit()
        else:
            chars = sys.argv[argument+1]

for iteration in range((len(chars) ** len(positions))):
    assignValues(positions, iteration)
    print("{0} {1}".format(
        getStr(positions),
        hashlib.md5(getStr(positions).encode('utf-8')).hexdigest()))
import random, sys

lengthProvided = False
charSetCount = 0

for argument in sys.argv:
    if argument.isdigit():
        length = int(argument)
        lengthProvided = True
    elif "-" in argument:
        charSetCount += 1

if not lengthProvided:
    print("Please provide a length, as well as a character set, in the form of command line arguments:\
\n-c\tCapital letters\n-l\tLowercase letters\n-n\tNumbers")
    exit()

if charSetCount == 0:
    print("Please provide at least one character set, in the form of a command line argument:\
\n-c\tCapital letters\n-l\tLowercase letters\n-n\tNumbers")
    exit()

if charSetCount > length:
    print("Too many character sets for the given length, please try again with \
a higher length or fewer character sets.")
    exit()

willContainCapitals  = True if "-c" in sys.argv else False
willContainLowercase = True if "-l" in sys.argv else False
willContainNumbers   = True if "-n" in sys.argv else False

capitals      = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
lowercase     = 'abcdefghijklmnopqrstuvwxyz'
numbers       = '0123456789'
capitalsList  = [char for char in capitals]
lowercaseList = [char for char in lowercase]
numbersList   = [char for char in numbers]

def password(length, willContainCapitals, willContainLowercase, willContainNumbers):
    """Returns a "password" string generated based on the parameters
    given.

    :param length: The length of the string.

    :param willContainCapitals: Whether or not the string will contain
    capital letters.

    :param willContainLowercase: Whether or not the string will
    contain lowercase letters.

    :param willContainNumbers: Whether or not the string will contain
    digits.

    :returns: The generated string.

    """
    validCharacters = []
    finalPassword   = []
    if willContainCapitals:  validCharacters += capitals
    if willContainLowercase: validCharacters += lowercase
    if willContainNumbers:   validCharacters += numbers
    for x in range(length):
        finalPassword.append(random.choice(validCharacters))
    return "".join(finalPassword)

def containsCharFromCharSet(password, charSet):
    """Returns whether or not a string contains at least one character
    from a given list of chars.  This is used to make sure that a
    generated password satisfies all of the criteria that it needs to.

    :param password: The input string.

    :param charSet: The list of characters to check against.

    :returns: True if the string contains a character from the char
    list, False if not.

    """
    for char in charSet:
        if char in password:
            return True
    return False

thePassword = password(length, willContainCapitals, willContainLowercase, willContainNumbers)

done = False

while not done:
    if willContainCapitals:
        if not containsCharFromCharSet(thePassword, capitalsList):
            thePassword = password(length, willContainCapitals, willContainLowercase, willContainNumbers)
            continue
    if willContainLowercase:
        if not containsCharFromCharSet(thePassword, lowercaseList):
            thePassword = password(length, willContainCapitals, willContainLowercase, willContainNumbers)
            continue
    if willContainNumbers:
        if not containsCharFromCharSet(thePassword, numbersList):
            thePassword = password(length, willContainCapitals, willContainLowercase, willContainNumbers)
            continue
    done = True

print(thePassword)

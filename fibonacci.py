import time

def fibonacci():
    """Prints the Fibonacci sequence, with each number on its own line,
    and waits for 100ms before printing the next number in the
    sequence.

    """
    x = 1
    y = 0
    while True:
        x = x + y
        y = x + y
        print(x)
        print(y)
        time.sleep(0.1)

fibonacci()

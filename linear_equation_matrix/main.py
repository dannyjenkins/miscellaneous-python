from fractions import Fraction

matrix    = []
rows      = 0
columns   = 0
prev_oper = []

"""The length of the longest matrix entry, in terms of characters."""
longest_in_chars = 0

def is_valid_fraction(input: str):
    if input[0] == '/' or input[len(input) - 1] == '/':
        return False
    for x in range(len(input)):
        if input[x] not in ['1', '2', '3', '4', '5',
                            '6', '7', '8', '9', '0', '/', '-']:
            return False
    return True

def opr_to_str(opr: str, row1: int, row2: int = 0, const: str = "1/1"):
    if opr == "add row":
        if const == "1/1" or const == "1":
            return "R{} += R{}".format(str(row1), str(row2))
        return "R{} += {} * R{}".format(str(row1), const, str(row2))

    elif opr == "swap":
        return "R{} <-> R{}".format(str(row1), str(row2))

    elif opr == "const":
        return "R{} *= {}".format(str(row1), const)

def check_input(the_input, valid_choices, is_int: bool = True):
    message = "Invalid input, try again: "
    while the_input not in valid_choices:
        if is_int: the_input = int(input(message)) - 1
        else:      the_input = input(message)


def initialize_matrix():
    global rows
    global columns

    rows = input("Input number of rows: ")
    while not rows.isdigit() or len(rows) == 0:
        rows = input("Invalid input, try again: ")
    rows = int(rows)
        
    columns = input("Input number of columns: ")
    while not columns.isdigit() or len(columns) == 0:
        columns = input("Invalid input, try again: ")
    columns = int(columns)

    for x in range(rows):
        matrix.append([])

    for x in range(rows):
        for y in range(columns):
            matrix[x].append(Fraction("0"))


def set_longest_in_chars():
    """Find and set the length of the longest matrix entry, in terms of
    characters, for later use in alignment when printing the
    matrix.

    """
    global longest_in_chars
    for x in range(rows):
        for y in range(columns):
            formatted_frac = format_frac(matrix[x][y])
            if len(formatted_frac) > longest_in_chars:
                longest_in_chars = len(formatted_frac)


def input_values():
    for x in range(rows):
        for y in range(columns):
            the_input: str = input(f"Input value for {x+1},{y+1}: ")
            matrix[x][y] = Fraction(the_input)


def print_matrix():
    set_longest_in_chars()
    print()
    for x in range(rows):
        for y in range(columns):
            formatted_entry = format_frac(matrix[x][y])
            entry_length    = len(formatted_entry)
            spaces          = " " * (longest_in_chars - entry_length)
            print(f"{spaces}{formatted_entry}", end = " ")
        print()
    print()


def format_frac(input: Fraction):
    if input.denominator == 1:
        return str(input.numerator)
    return str(input.numerator) + "/" + str(input.denominator)


def swap_rows(one: int, two: int):
    matrix[one], matrix[two] = matrix[two], matrix[one]


def swap_rows_input():
    global prev_oper
    global rows

    if rows == 2:
        swap_rows(0, 1)
        prev_oper.append(opr_to_str("swap", 1, 2))
    else:
        one = int(input("Enter first row to be swapped: "))
        index_of_one = one - 1
        check_input(index_of_one, range(rows))

        two = int(input("Enter second row to be swapped: "))
        while two == one:
            two = int(input("The two rows cannot be the same. Try again: "))

        index_of_two = two - 1
        check_input(index_of_two, range(rows))

        swap_rows(index_of_one, index_of_two)
        prev_oper.append(opr_to_str("swap", one, two))
    print_matrix()


def add_multiple(one: int, two: int, multiple: str):
    for col in range(len(matrix[one])):
        matrix[one][col] += (matrix[two][col] * Fraction(multiple))


def add_multiple_input():
    global prev_oper
    one = int(input("Enter row to be modified: "))
    index_of_one = one - 1
    check_input(index_of_one, range(rows))

    two = int(input("Enter row to get values from: "))
    while two == one:
        two = int(input("The two rows cannot be the same. Try again: "))

    index_of_two = two - 1
    check_input(index_of_two, range(rows))

    multiple = input("Enter multiple: ")
    while not is_valid_fraction(multiple):
        multiple = input("Invalid input, try again: ")
    
    add_multiple(index_of_one, index_of_two, multiple)
    prev_oper.append(opr_to_str("add row", one, two, multiple))
    print_matrix()
    

def multiply_by_constant(row: int, constant: str):
    for col in range(len(matrix[row])):
        matrix[row][col] *= Fraction(constant)


def multiply_by_constant_input():
    global prev_oper
    row = int(input("Enter row to be multiplied: "))
    index_of_row = row - 1
    check_input(index_of_row, range(rows))

    constant = input("Enter constant: ")
    while not is_valid_fraction(constant):
        constant = input("Invalid input, try again: ")

    multiply_by_constant(index_of_row, constant)
    prev_oper.append(opr_to_str("const", row, const = constant))
    print_matrix()


def loop():
    while True:

        print("Previous operations: ", end = "")
        if len(prev_oper) == 0:
            print("None")
        else:
            print(", ".join(prev_oper))

        print("Enter choice:")
        print("1. Swap two rows")
        print("2. Add a multiple of a row to another row")
        print("3. Multiply a row by a constant")
        print("4. Quit")

        choice = input()
        check_input(choice, ["1", "2", "3", "4"], False)

        if   choice == "1": swap_rows_input()
        elif choice == "2": add_multiple_input()
        elif choice == "3": multiply_by_constant_input()
        elif choice == "4": break


initialize_matrix()
input_values()
print_matrix()
loop()

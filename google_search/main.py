import webbrowser
import subprocess

"""
Run a given shell command.
"""
def run_command(command: str):
    return subprocess.Popen(command,
                            stdout=subprocess.PIPE,
                            shell=True)

# workspace name
wsn = "1: www"

# Input query from rofi
rofi = run_command("rofi -dmenu")

query = rofi.communicate()[0].decode('utf-8')[:-1]

if query == "":
    exit()

# change workspace
chws = run_command("i3-msg \"workspace " + wsn + "\"")

# Google query
gquery = "https://www.google.com/search?q=" + query

webbrowser.open(gquery)

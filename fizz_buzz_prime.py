def isprime(x):
    """Returns whether or not the input integer is a prime number.

    :param x: The input integer.

    :returns: Whether or not it is a prime number.

    """
    result = []
    for num in range(2,x):
        if x % num == 0:
            result.append(0)
            break
    return not len(result) == 1

primes = []

def fizzbuzzprime(reach):
    """Prints a sequence of numbers, and prints Fizz and Buzz in
    alternating order next to each of the primes that appear.

    :param reach: How high the function should count.

    """
    for test in range(reach):
        if test in [0, 1]:
            print(test)
        elif isprime(test):
            primes.append(test)
            if primes.index(test) % 2 == 0:
                print(test, "\tFizz")
            else:
                print(test, "\tBuzz")
        else:
            print(test)

fizzbuzzprime(int(input("How high do you want me to count? ")) + 1)

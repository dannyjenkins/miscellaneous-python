# December 28, 2021

import math


def mandelbrot(input: complex, c: complex):
    """Iteration formula"""
    return (input ** 2) + c


def absolute_value(input: complex):
    """Calculate the absolute value of a complex number."""
    return math.sqrt((input.real ** 2) + (input.imag ** 2))


def is_in_set(input: complex):
    """Return whether or not the input complex number may be in the
    Mandelbrot set."""
    return absolute_value(input) < 2


# The number of iterations for each calculation.
iterations = 2000

# The resolution of the resulting output.  A given value x will result
# in an output of vertical resolution 2x + 1 when using the default
# border values, i.e. displaying the entire Mandelbrot set.
res = 40

top_border    = -res
bottom_border = res + 1
left_border   = -(res * 2)
right_border  = res + 1

# Each line
for line in range(top_border, bottom_border):

    # Each block in the line
    for block in range(left_border, right_border):

        imag_part = line
        real_part = block

        z = 0 + 0j
        c = complex((real_part / res), (imag_part / res))

        for iter in range(iterations):
            z = mandelbrot(z, c)
            if not is_in_set(z): break

        if is_in_set(z): print("\x1b[6;30;47m  \x1b[0m", end = "")
        else:            print("  ", end = "")

    print("")

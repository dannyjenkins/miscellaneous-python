
def triangularnumber(x):
    """Finds the nth triangular number of the input integer, and prints
    the entire addition sequence along with said number at the end.

    :param x: The input integer.

    """
    total = 0
    numbers = []

    while not x.isdigit():
        x = input("That's not a number. Try again: ")

    x = int(x)

    while x < 0:
        x = int(input("That's not a positive number. Try again: "))

    while x >= 1:
        numbers.append(x)
        x -= 1

    for x in numbers:
        total += x

    print(f"{' + '.join(map(str,numbers))} = {total}")

triangularnumber(input("Enter a positive number: "))

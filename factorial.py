import sys


def factorial(x):
    """Take an input integer, and prints the factorial multiplication
    sequence as well as the answer at the end.

    :param x: The input integer.

    """
    numbers = []

    if not x.isdigit():
        print("Error: argument is not a valid number")
        exit()

    x = int(x)

    if x < 0:
        print("Error: argument is not a positive number")
        exit()

    while x >= 1:
        numbers.append(x)
        x -= 1

    total = numbers[0]
    for x in numbers[1:-1]:
        total *= x

    print(f"{' * '.join(map(str,numbers))} = {total}")


if len(sys.argv) == 1:
    print("Error: numerical argument not provided")
    exit()

argument = sys.argv[1]

factorial(argument)

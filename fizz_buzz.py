def FizzBuzz(x):
    for y in range(x + 1):
        if   y % 3 == 0 and y % 5 == 0: print("FizzBuzz")
        elif y % 5 == 0:                print("Buzz")
        elif y % 3 == 0:                print("Fizz")
        else:                           print(y)

FizzBuzz(1000000)

def add(row1, row2, array):
    """Add a matrix row to another, mod 2.

    :param row1: The first row, which will remain unchanged
    :param row2: The second row, which will take on a new value
    """
    new_row = []
    for x in range(len(array[row1])):
        new_row.append(array[row1][x] ^ array[row2][x])
    array[row2] = new_row


def swap(row1, row2, array):
    """Swap two rows in the equation system.

    """
    array[row1], array[row2] = array[row2], array[row1]


def rref(array):
    """Reduce the equation system to row echelon form.  Credit to
    Johan Magnus Engevik for this implementation.

    :param array: The equation system, in matrix form
    """
    for j in range(len(array[0]) - 1):
        print(j)
        if j == 491: break
        if array[j][j] == 0:
            found_1 = False
            u = j + 1
            while not found_1 and u < len(array):
                if array[u][j] == 1:
                    swap(j, u, array)
                    found_1 = True
                u += 1

            if not found_1:
                return "No unique solution."

        for u in range(j + 1, len(array)):
            if array[u][j] == 1:
                add(j, u, array)

    for j in range(len(array[0]) - 2, -1, -1):
        print(j)
        u = len(array) - 1
        while array[u][j] == 0:
            u -= 1
        for i in range(u - 1, -1, -1):
            if array[i][j] == 1:
                add(u, i, array)

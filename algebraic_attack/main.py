import numpy as np
from lfsr import run_lfsr
from typing import List
import csv

key_stream = []
with open('key_stream.csv', newline='') as csvfile:
    the_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in the_reader:
        key_stream.append(row)
key_stream = key_stream[0]
for x in range(len(key_stream)):
    key_stream[x] = int(key_stream[x])


def F(x1: int, x2: int, x3: int, x4: int, x5: int, x6: int) -> int:
    return x1 ^ x6 ^ (x2 * x4) ^ (x3 * x5)


def sum_dot_product_solution(bitgroup5: List[int], index: int):
    """Calculate the nth final output bit.

    In the final, row reduced system of equations, there are five
    columns of free variables.  This function calculates the dot
    product between these free variables and a given 5-bit group, and
    adds the result to the solution (the very right column).  This is
    then the (31-n)th bit of the initial state of the LFSR (since the
    LFSR state operates in reverse).

    :param bitgroup5: An arbitrary 5-bit group
    :param index: The index of the equation to calculate

    :returns: The XOR of the dot product and the solution
    """
    free_vars    = equations[index][-6:-1]
    dot_product  = np.matmul(bitgroup5, free_vars) % 2
    solution     = equations[index][-1]
    return_value = (dot_product + solution) % 2
    return return_value


equations = [
]

with open('row_reduced_matrix_small.csv', newline='') as csvfile:
    the_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in the_reader:
        equations.append(row)
for x in range(len(equations)):
    for y in range(len(equations[x])):
        equations[x][y] = int(equations[x][y])

lfsr_state = []
lfsr_taps  = [0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,1]

for x in range(len(equations)):
    lfsr_state.append(sum_dot_product_solution([0,1,0,1,0], x))

lfsr_state.reverse()

my_stream = []

for x in range(491):
    my_stream.append(F(
        lfsr_state[0],
        lfsr_state[5],
        lfsr_state[10],
        lfsr_state[19],
        lfsr_state[23],
        lfsr_state[30],
    ))
    run_lfsr(lfsr_state, lfsr_taps)

for x in key_stream:
    print(x, end="")
print()

for x in my_stream:
    print(x, end="")
print()

print(my_stream == key_stream)

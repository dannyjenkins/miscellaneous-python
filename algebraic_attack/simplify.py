from typing import List
import itertools as it
import csv

var_list = []
with open('new_s_values.csv', newline='') as csvfile:
    the_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in the_reader:
        var_list.append(row)


def flatten(input_list):
    """Flatten a list of lists into a list of depth 1."""
    if input_list == []:
        return input_list

    if isinstance(input_list[0], list):
        return flatten(input_list[0]) + flatten(input_list[1:])

    return input_list[:1] + flatten(input_list[1:])


def split_into_list(input: str):
    """Split a product into a list of its factors.

    "s0s1s2" -> ["s0", "s1", "s2"]

    :param input: a product, represented as a string
    :returns: A list of its factors; list of strings
    """
    return_list = []

    while input != "":
        z = 2
        while True:
            current = input[0:z]
            if len(input) == z:
                return_list.append(input)
                input = input[z:]
                break
            if input[0:z][-1] != 's':
                z += 1
                continue
            return_list.append(current[:-1])
            input = input[z - 1:]
            z = 2

    return return_list


def gen_representations(input: str):
    """Generate a list of representations for a product.

    A product may be represented in several ways, e.g. "s0s1s2" can be
    represented as "s1s0s2" and "s2s0s1" among others.  This function
    generates and returns a list of all of them for a given input
    product.
    """
    input_list = split_into_list(input)
    permut     = list(it.permutations(input_list, len(input_list)))
    permut_str = []

    for x in range(len(permut)):
        new_string = ""
        for y in permut[x]:
            new_string += y
        permut_str.append(new_string)

    return permut_str


def representation_in_list(input: str, the_list: List[str]) -> bool:
    """Check if a product is in a list.

    Return whether a product, or any of its representations, exist
    in a given list.

    "s0s1", ["s0s1", "s2", "s3"] -> True
    "s0s1", ["s1s0", "s2", "s3"] -> True
    "s0s2", ["s1s0", "s2", "s3"] -> False
    """
    representations = gen_representations(input)
    for x in representations:
        if x in the_list:
            return True
    return False


def remove_duplicates(input):
    """Remove duplicates from an input.

    Remove duplicates from an input, leaving only one of each element
    in the input.

    The input can be either a string or a list.

    In the string case, the function will remove duplicate factors
    from a product.

    "s0s1s0s2" -> "s0s1s2"

    In the list case, the function will remove duplicate products from
    a list, taking different representations into consideration.

    ["s0s1", "s1s0", "s3"] -> ["s0s1", "s3"]
    """
    if type(input) == list:
        with_dupes = input
        return_value = []

        for x in with_dupes:
            if not representation_in_list(x, return_value):
                return_value.append(x)

        return return_value

    with_dupes = split_into_list(input)

    without_dupes = []
    for x in with_dupes:
        if x not in without_dupes:
            without_dupes.append(x)

    return "".join(without_dupes)


def _multiply_two_brackets(bracket1, bracket2):
    """Multiply two brackets together.

    ["s0", "s1"], ["s2", "s3"] -> ["s0s2", "s0s3", "s1s2", "s1s3"]
    """
    return_list = []

    for x in bracket1:
        for y in bracket2:
            return_list.append(x + y)
            
    return return_list


def remove_evens(input: str, the_list: List[str]):
    """Remove all instances of a given product from a list.

    Meant to be used when there are an even number of the given
    product in the list.  Takes different representations into
    consideration.

    "s0s1", ["s0s1", "s1s0", "s2", "s3"] -> ["s2", "s3"]
    """
    representations = gen_representations(input)
    for x in range(len(the_list) - 1, -1, -1):
        if the_list[x] in representations:
            del the_list[x]


def remove_odds(input: str, the_list: List[str]):
    """Remove all but one instance of a given product from a list.

    Meant to be used when there are an odd number of the given
    product in the list.  Takes different representations into
    consideration.

    "s0s1", ["s0s1", "s1s0", "s1s0", "s2",] -> ["s0s1", "s2"]
    """
    representations = gen_representations(input)
    representations_count = 0
    for x in the_list:
        for y in representations:
            if y == x:
                representations_count += 1

    for x in range(len(the_list) - 1, -1, -1):
        if the_list[x] in representations:
            del the_list[x]
            representations_count -= 1
            if representations_count == 1:
                break


def count_occurrences(input: str, the_list: List[str]):
    """Count the number of occurrences of a product in a list.

    Takes different representations into consideration.

    "s0s1", ["s0s1", "s1s0", "s1s0", "s2", "s3"] -> 3
    """
    representations = gen_representations(input)
    representations_count = 0
    for x in the_list:
        if x in representations:
            representations_count += 1
    return representations_count


def remove_evens_odds(the_list):
    """Remove pairs of duplicate elements from a list.

    For every element in the list:

        - if there are an even number of this element in the list,
          remove all occurrences of it

        - if there are an odd number of this element in the list,
          remove all but one occurrence of it
    """
    for x in range(len(the_list) - 1, -1, -1):
        if x >= len(the_list) - 1:
            continue
        if count_occurrences(the_list[x], the_list) > 1:
            if count_occurrences(the_list[x], the_list) % 2 == 0:
                remove_evens(the_list[x], the_list)
            else:
                remove_odds(the_list[x], the_list)


def multiply_brackets(*brackets):
    """Multiply an arbitrary number of brackets together.

    :returns: A list of all products
    """
    brackets = list(brackets)
    if len(brackets) == 1:
        for x in brackets[0]:
            if type(x) == list:
                brackets = brackets[0]
                break
        else:
            brackets = brackets[0]

    while len(brackets) > 1:

        first  = brackets[0]
        second = brackets[1]

        if type(first) != list:
            first = [brackets[0]]
        if type(second) != list:
            second = [brackets[1]]

        multiplied = _multiply_two_brackets(first, second)
        brackets = brackets[2:]
        brackets.insert(0, multiplied)

    return brackets[0]


def replace_products(input_list):
    for x in range(len(input_list)):
        indices = [int(y[1:]) for y in split_into_list(input_list[x])]
        if len(indices) == 1:
            continue
        new_value = var_list[indices[0]][indices[1]]
        input_list[x] = new_value



def simplify(input_list):
    """Simplify an expression.

    Chain together multiple functions defined in this file to fully
    simplify an expression.
    """
    input_list = flatten(input_list)
    for x in range(len(input_list)):
        input_list[x] = remove_duplicates(input_list[x])
    remove_evens_odds(input_list)
    replace_products(input_list)
    return input_list

# 5fa326ca59860a63c683cb32e8ecff9d

#lfsr_state = [1,0,0]
#lfsr_taps  = [0,0,1]
#output     = len(lfsr_state) - 1
#
#if len(lfsr_state) != len(lfsr_taps):
#    raise Exception("The lfsr_state list and the " +
#                    "lfsr_taps list must be of equal length.")

def run_lfsr(state, taps):
    """Clock the LFSR once.

    :param state: The current state of the LFSR
    :param taps: The LFSR taps

    """
    prepend_value = 0
    for count, value in enumerate(taps):
        if value == 1:
            prepend_value = prepend_value ^ state[count]
    state.insert(0, prepend_value)
    del state[len(state) - 1]

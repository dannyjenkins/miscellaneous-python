from simplify import multiply_brackets
from simplify import flatten
from simplify import remove_evens_odds
from simplify import simplify
import csv

var_names = []
with open('var_names.csv', newline='') as csvfile:
    the_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in the_reader:
        var_names.append(row)
var_names = var_names[0][:-1]


key_stream = []
with open('key_stream.csv', newline='') as csvfile:
    the_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in the_reader:
        key_stream.append(row)
key_stream = key_stream[0]
for x in range(len(key_stream)):
    key_stream[x] = int(key_stream[x])

lfsr_state = [ "s30","s29", "s28", "s27","s26","s25", "s24",
               "s23","s22","s21", "s20", "s19","s18","s17", "s16",
               "s15","s14","s13", "s12", "s11","s10","s9", "s8",
               "s7","s6","s5", "s4", "s3","s2","s1", "s0", ]
lfsr_taps  = [0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,1]

if len(lfsr_state) != len(lfsr_taps):
    raise Exception("The lfsr_state list and the " +
                    "lfsr_taps list must be of equal length.")


simplified_equations = []
matrix = []


def run_lfsr(state, taps):
    """Clock the LFSR once.

    :param state: The current state of the LFSR
    :param taps: The LFSR taps

    """
    prepend_value = []
    for count, value in enumerate(taps):
        if value == 1:
            prepend_value.append(state[count])
    prepend_value = flatten(prepend_value)
    remove_evens_odds(prepend_value)
    state.insert(0, prepend_value)
    del state[len(state) - 1]


def get_equation(state):
    """Get the equation in unknowns from the current LFSR state.

    F(x1,x2,x3,x4,x5,x6) = x1 + x6 + x2x4 + x3x5

    Take the corresponding unknown s-values from the LFSR and feed it
    into the F function, creating a non-simplified expression
    (meaning, may contain brackets and redundant entries) for the
    output bit expressed by XOR and multiplication actions on LFSR
    values.  Used to generate the matrix of equations which will be
    row reduced and solved later.

    :param state: The current LFSR state

    :returns: a non-simplified equation in unknowns s30,s29,...,s0
    """
    x1 = state[0]
    x2 = state[5]
    x3 = state[10]
    x4 = state[19]
    x5 = state[23]
    x6 = state[30]
    return_value = [x1, x6]
    return_value = flatten(return_value)
    remove_evens_odds(return_value)
    return_value.append([x2, x4])
    return_value.append([x3, x5])
    return return_value


def run(current_x):
    """Simplify an equation in unknowns and add it to the system.

    Get the equation in unknowns from the current state of the LFSR,
    simplify it, and add it to the system of equations, printing
    various output along the way.

    :param current_x: a string, e.g. "x0" or "x5"
    """
    print(f"{current_x} State:               {lfsr_state}")
    print(f"{current_x} Equation:            {get_equation(lfsr_state)}")

    current_eq = get_equation(lfsr_state)
    for x in range(len(current_eq)):
        if type(current_eq[x]) == list:
            current_eq[x] = multiply_brackets(current_eq[x])

    current_eq = simplify(current_eq)
    print(f"{current_x} Simplified equation: {current_eq}")

    simplified_equations.append(current_eq)

    run_lfsr(lfsr_state, lfsr_taps)


def create_matrix_row(simplified_eq, solution):
    """Create a row for the augmented matrix of equations.

    Creates a list, representing the nth equation in the system.  Each
    index represents a specific s-value; the ordering of these can be
    found in var_names.csv.  Each value will be a 1 or a 0, a 1
    meaning that this specific s-value does appear in the equation,
    and a 0 meaning that it does not.  The very last bit in each row
    is the solution bit.

    :param simplified_eq: A simplified list of terms, meaning that all
        brackets have been multiplied, and that there are no redundant
        terms etc.  and that all of the terms will be XORed together
        to create the solution
    :param solution: The solution to this specific equation

    :returns: A list of 0,1-coefficients, and the solution bit at the
              very end
    """
    return_value = []
    for x in var_names:
        if x in simplified_eq:
            return_value.append(1)
        else:
            return_value.append(0)
    return_value.append(solution)
    return return_value


def create_matrix():
    """Generate the full augmented matrix."""
    for index, value in enumerate(simplified_equations):
        matrix.append(create_matrix_row(value, key_stream[index]))


for x in range(491):
    current = "x" + str(x)
    run(current)

create_matrix()

import csv


var_list = []
with open('s-values.csv', newline='') as csvfile:
    the_reader = csv.reader(csvfile, delimiter=',', quotechar='|')
    for row in the_reader:
        var_list.append(row)


def replace_horizontal(start: int, x: int, y: int):
    """Replace products with new variables horizontally, above the
    diagonal.

    Starting from the specified x,y-coordinate, replace products with
    new variables until the very right column is reached.  The value
    will be incremented for each substitution.

    :param start: The starting value
    :param x: The starting x-coordinate
    :param y: The starting y-coordinate
    """
    global var_list

    for x in range(x, len(var_list[x])):
        new_value = 's' + str(start)
        var_list[y][x] = new_value
        start += 1


def replace_vertical(start: int, x: int, y: int):
    """Replace products with new variables vertically, below the
    diagonal.

    Starting from the specified x,y-coordinate, replace products with
    new variables until the very bottom row is reached.  The value
    will be incremented for each substitution.

    :param start: The starting value
    :param x: The starting x-coordinate
    :param y: The starting y-coordinate
    """
    global var_list

    for x in range(x, len(var_list)):
        new_value = 's' + str(start)
        var_list[x][y] = new_value
        start += 1


replace_horizontal(31, 1, 0)
replace_horizontal(31+30, 2, 1)
replace_horizontal(31+30+29, 3, 2)
replace_horizontal(31+30+29+28, 4, 3)
replace_horizontal(31+30+29+28+27, 5, 4)
replace_horizontal(31+30+29+28+27+26, 6, 5)
replace_horizontal(31+30+29+28+27+26+25, 7, 6)
replace_horizontal(31+30+29+28+27+26+25+24, 8, 7)
replace_horizontal(31+30+29+28+27+26+25+24+23, 9, 8)
replace_horizontal(31+30+29+28+27+26+25+24+23+22, 10, 9)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21, 11, 10)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20, 12, 11)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19, 13, 12)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18, 14, 13)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17, 15, 14)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16, 16, 15)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15, 17, 16)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14, 18, 17)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13, 19, 18)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12, 20, 19)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11, 21, 20)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10, 22, 21)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9, 23, 22)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9+8, 24, 23)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9+8+7, 25, 24)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9+8+7+6, 26, 25)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9+8+7+6+5, 27, 26)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9+8+7+6+5+4, 28, 27)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9+8+7+6+5+4+3, 29, 28)
replace_horizontal(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9+8+7+6+5+4+3+2, 30, 29)

replace_vertical(31, 1, 0)
replace_vertical(31+30, 2, 1)
replace_vertical(31+30+29, 3, 2)
replace_vertical(31+30+29+28, 4, 3)
replace_vertical(31+30+29+28+27, 5, 4)
replace_vertical(31+30+29+28+27+26, 6, 5)
replace_vertical(31+30+29+28+27+26+25, 7, 6)
replace_vertical(31+30+29+28+27+26+25+24, 8, 7)
replace_vertical(31+30+29+28+27+26+25+24+23, 9, 8)
replace_vertical(31+30+29+28+27+26+25+24+23+22, 10, 9)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21, 11, 10)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20, 12, 11)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19, 13, 12)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18, 14, 13)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17, 15, 14)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16, 16, 15)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15, 17, 16)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14, 18, 17)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13, 19, 18)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12, 20, 19)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11, 21, 20)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10, 22, 21)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9, 23, 22)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9+8, 24, 23)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9+8+7, 25, 24)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9+8+7+6, 26, 25)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9+8+7+6+5, 27, 26)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9+8+7+6+5+4, 28, 27)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9+8+7+6+5+4+3, 29, 28)
replace_vertical(31+30+29+28+27+26+25+24+23+22+21+20+19+18+17+16+15+14+13+12+11+10+9+8+7+6+5+4+3+2, 30, 29)


for x in var_list:
    print(",".join(x))


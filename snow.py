import os
import random
import time

termdimensions = os.popen('stty size', 'r').read().split()
pregenlines    = []
previouslines  = []

answer = input("Rain or snow?").lower()
while answer not in ["rain", "snow"]:
    answer = input("That's not a valid choice. Please try again: ").lower()

rainparticles = ["|", "!", "."]
for x in range(30):
    rainparticles.append(" ")

snowparticles = [".", ",", "x", "*"]
for x in range(30):
    snowparticles.append(" ")

def row(rain=False):
    """Generate a string with random characters which looks like
    precipitation.

    :param rain: Whether or not to use the character set pertaining to
    rain. Defaults to false, and will use the character set pertaining
    to snow instead unless specified otherwise.

    :returns: a string with random characters which looks like
    precipitation.

    """
    line = []
    for x in range(int(termdimensions[1])):
        if answer == "rain":
            line.append(random.choice(rainparticles))
        elif answer == "snow":
            line.append(random.choice(snowparticles))
    stringline = "".join(str(x) for x in line)
    return stringline

for x in range(int(termdimensions[0]) * 10):
    pregenlines.append(str(row()))

while True:
    previouslines.insert(0, str(random.choice(pregenlines)))
    os.system("clear")
    if len(previouslines) >= int(termdimensions[0]):
        del previouslines[-1]
    print('\x1b[1A\x1b[2K' + "\n".join(previouslines))
    if answer == "rain":
        time.sleep(0.05)
    elif answer == "snow":
        time.sleep(0.1)

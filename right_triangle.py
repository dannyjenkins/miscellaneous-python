one = int(input("First cathetus:\t\t"))
two = int(input("Second cathetus:\t"))
print(f"Hypothenuse:\t\t{((one ** 2) + (two ** 2)) ** 0.5}")
print(f"Surface area:\t\t{(one * two) / 2}")

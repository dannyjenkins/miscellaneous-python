# A simple script that takes two times of the day as input (in 24 hour
# format), and calculates how far apart they are in time.

import sys

"""Converts a number of hours and minutes into the corresponding
number of minutes.

0:01 -> 1
0:59 -> 59
1:00 -> 60
2:00 -> 120
"""
def hours_and_minutes_to_minutes(input: str):
    time_units        = input.split(':')
    hours:        int = int(time_units[0])
    minutes:      int = int(time_units[1])
    return_value: int = (hours * 60) + minutes
    return return_value


"""Converts a number of minutes into the corresponding
number of hours and minutes.

1   -> 0:01
59  -> 0:59
60  -> 1:00
120 -> 2:00
"""
def minutes_to_hours_and_minutes(input: int):
    hours:   str = str(input // 60)
    minutes: str = str(input %  60).zfill(2)
    return hours + ':' + minutes


if len(sys.argv) != 3:
    print("ERROR: Invalid input.")
    exit()

time1 = sys.argv[1]
time2 = sys.argv[2]

time1min = hours_and_minutes_to_minutes(time1)
time2min = hours_and_minutes_to_minutes(time2)

if time1min <= time2min:
    differencemin = time2min - time1min
else:
    differencemin = 1440 - time1min + time2min

difference = minutes_to_hours_and_minutes(differencemin)

print(difference)

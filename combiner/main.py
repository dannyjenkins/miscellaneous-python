from geffe import *
import itertools as it
import key_stream as ks

bitgroups13 = [list(x) for x in
               list(it.product([1,0], repeat=13))]
bitgroups13.reverse()

geffe_gen = GeffeGenerator(
    x1state = [1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0], # correct
    x1taps  = [0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1],
    x2state = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    x2taps  = [0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 1],
    x3state = [0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1], # correct
    x3taps  = [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1],
)

for value in bitgroups13:
    old_value = value.copy()
    geffe_gen.x1.reset()
    geffe_gen.x2.reset()
    geffe_gen.x3.reset()
    geffe_gen.x2.set_state(value)
    output = geffe_gen.generate_output_list(200)
    if output == ks.key_stream:
        print("True", old_value, output)
        break

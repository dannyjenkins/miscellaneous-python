from lfsr import *

class GeffeGenerator:
    def __init__(self, x1state, x1taps, x2state, x2taps, x3state, x3taps):
        self.x1 = LFSR(x1state, x1taps)
        self.x2 = LFSR(x2state, x2taps)
        self.x3 = LFSR(x3state, x3taps)

    def F(self) -> int:
        x1l = self.x1.get_last()
        x2l = self.x2.get_last()
        x3l = self.x3.get_last()
        return (x1l * x2l) ^ (x3l * (x2l ^ 1))

    def clock(self) -> int:
        return_value = self.F()
        self.x1.clock()
        self.x2.clock()
        self.x3.clock()
        return return_value

    def generate_output_list(self, length: int):
        """Run the Geffe generator, and produce a list of output bits.

        :param length: The length of the list
        """
        return_list: List[int] = []
        for _ in range(length):
            return_list.append(self.clock())
        return return_list

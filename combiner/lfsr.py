from typing import List

class LFSR:
    def __init__(self, state, taps):
        if len(state) != len(taps):
            raise Exception(
                "The length of the register (state) must be of the" + 
                " same length as the taps list (taps)"
            )
        
        for value in state:
            if value not in [0,1]:
                raise Exception(
                    "The register (state) cannot contain values" +
                    " which are not either 0 or 1"
                )

        for value in taps:
            if value not in [0,1]:
                raise Exception(
                    "The generating polynomial (taps) cannot" +
                    " contain values which are not either 0 or 1"
                )

        self.state          = state
        self.taps           = taps
        self.ORIGINAL_STATE = state.copy()


    def get_value(self, index: int) -> int:
        return self.state[index]


    def get_last(self) -> int:
        return self.state[-1]


    def set_state(self, new_state):
        if len(new_state) != len(self.state):
            raise Exception("The new state must be of the same" +
                            " length as the old state")
        for value in new_state:
            if value not in [0 ,1]:
                raise Exception("The new state can only consist" +
                                " of values 0 or 1")
        self.state = new_state


    def reset(self):
        self.state = self.ORIGINAL_STATE.copy()


    def clock(self):
        prepend_value = 0
        for count, value in enumerate(self.taps):
            if value == 1:
                prepend_value = prepend_value ^ self.state[count]
        self.state.insert(0, prepend_value)
        del self.state[len(self.state) - 1]


    def generate_output_list(self, length: int) -> List[int]:
        return_list: List[int] = []
        for _ in range(length):
            return_list.append(self.get_last())
            self.clock()
        return return_list

# 5fa326ca59860a63c683cb32e8ecff9d

from typing import List
import itertools


truth_table = []

# generating 5 bit sequences
bitgroups5 = [list(x) for x in
               list(itertools.product([1,0], repeat=5))]
bitgroups5.reverse()


def F(x1: int, x2: int, x3: int, x4: int, x5: int):
    """
    Implementation of F.
    """
    return ((x1 * x2) + x3 + x4 + x5) % 2


def F_to_str(x1: int, x2: int, x3: int, x4: int, x5: int):
    return (f"{x1} * {x2} XOR {x3} XOR {x4} XOR {x5} = "
            f"{F(x1, x2, x3, x4, x5)}")


def print_truth_table():
    for count, val in enumerate(truth_table):
        print(f"{count}:\t"
              f"{val[0]} * {val[1]} XOR {val[2]} XOR "
              f"{val[3]} XOR {val[4]} = "
              f"{F(val[0], val[1], val[2], val[3], val[4])}")


def gen_truth_table():
    for x in bitgroups5:
        truth_table.append([x[0], x[1], x[2], x[3], x[4],
                            F(x[0], x[1], x[2], x[3], x[4])])


def affine_truth_table(coefficients: List[int]):
    """Generate truth table for an affine function.

    :param coefficients: A binary list of length 6, containing
        coefficients for the affine function.

    :returns: The truth table values for the affine function
    """
    if len(coefficients) != 6:
        raise Exception("Input list must be of length 6")
    for x in coefficients:
        if x not in [0, 1]:
            raise Exception("Each coefficient must be binary")

    truth_table_output = []

    for group in bitgroups5:
        truth_value = 0
        for count, value in enumerate(group):
            if coefficients[count] == 1:
                truth_value ^= value
        truth_value ^= coefficients[5] # b
        truth_table_output.append(truth_value)

    return truth_table_output


def compare_affine_to_F(coefficients: List[int]):
    """Compare the truth table of an affine function to that of F, to
    find a good affine approximation.

    :param coefficients: A binary list of length 6, containing
        coefficients for the affine function.

    :returns: The number of equal values between the truth table of
              the affine function and that of F.
    """
    if len(coefficients) != 6:
        raise Exception("Input list must be of length 6")
    for x in coefficients:
        if x not in [0, 1]:
            raise Exception("Each coefficient must be binary")

    affine_table = affine_truth_table(coefficients)

    numerator = 0

    for count, value in enumerate(truth_table):
        if value[5] == affine_table[count]:
            numerator += 1

    return numerator


def find_best_approx():
    """Compare the affine functions to F, and find the best
    approximation.

    :returns: The coefficients of the best affine approximation, and
              the number of equal values between the truth table of
              this affine function and that of F.
    """
    best_approx = []
    best_count  = 0

    bitgroups6 = [list(x) for x in
                  list(itertools.product([1,0], repeat=6))]
    bitgroups6.reverse()
    bitgroups6 = bitgroups6[:1]
    #bitgroups6.remove([1,1,1,1,1,1]) # 3/4
    #bitgroups6.remove([0,0,1,1,1,0]) # 3/4
    #bitgroups6.remove([0,1,1,1,1,0]) # 3/4
    #bitgroups6.remove([1,0,1,1,1,0]) # 3/4

    for x in bitgroups6:
        current_count = compare_affine_to_F(x)
        if current_count > best_count:
            best_approx = x
            best_count  = current_count

    return [best_approx, best_count]

    



gen_truth_table()
print_truth_table()

my_coefficients   = [0, 0, 1, 1, 1, 0]
my_affine_results = affine_truth_table(my_coefficients)

for count, value in enumerate(my_affine_results):
    if value == truth_table[count][5]:
        print("\u001b[32m" + str(value) + "\u001b[0m")
    else:
        print(value)

print(compare_affine_to_F(my_coefficients))

best_approximation = find_best_approx()
print(best_approximation[0], best_approximation[1])

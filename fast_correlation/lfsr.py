# 5fa326ca59860a63c683cb32e8ecff9d

lfsr_state = [1,0,0]
lfsr_taps  = [0,0,1]
output     = len(lfsr_state) - 1

if len(lfsr_state) != len(lfsr_taps):
    raise Exception("The lfsr_state list and the " +
                    "lfsr_taps list must be of equal length.")

def run_lfsr(output_index):
    """Clock the LFSR once.

    :param output_index: The index of the output value once the LFSR
        has been clocked

    :returns: The value at the given index
    """
    global lfsr_state, lfsr_taps
    prepend_value = 0
    for count, value in enumerate(lfsr_taps):
        if value == 1:
            prepend_value = prepend_value ^ lfsr_state[count]
    lfsr_state.insert(0, prepend_value)
    del lfsr_state[len(lfsr_state) - 1]
    return lfsr_state[output_index]

print(lfsr_state[output], lfsr_state)

for x in range(16):
    print(run_lfsr(output), lfsr_state)

print()

import sys, itertools


# This script takes two arguments:
# Argument 1: XOR result
# Argument 2: Relay configuration
# And returns the resulting ciphertext
if len(sys.argv) != 3:
    raise Exception("Invalid number of arguments (should be 2)")
if (not sys.argv[1].isnumeric()) and \
   (not sys.argv[2].isnumeric()):
    raise Exception("Arguments need to be numerical")
if (not len(sys.argv[1]) == 5) and \
   (not len(sys.argv[2]) == 5):
    raise Exception("Arguments need to each be 5 digits long")
for x in sys.argv[1]:
    if x != '0' and x != '1':
        raise Exception("Arguments need to each be binary")
for x in sys.argv[2]:
    if x != '0' and x != '1':
        raise Exception("Arguments need to each be binary")

def relay(xor_result: str, relay: str) -> str:
    """Perform relay swaps on an XOR result.

    :param xor_result: The result after a plaintext character has been
        XORed with the binary 5-bit group on the wheels connected to
        the left of the machine
    :param relay: The 5-bit group on the wheels connected to the right
        of the machine

    :returns: A ciphertext character, after the relay swaps have been
              performed
    """
    if len(xor_result) != 5 or len(relay) != 5:
        raise Exception("Invalid input length")
    for char in xor_result:
        if char not in ['0', '1']:
            raise Exception("Invalid input string: not binary")
    for char in relay:
        if char not in ['0', '1']:
            raise Exception("Invalid input string: not binary")

    xor_list = list(xor_result)

    if relay[0] == '0':
        xor_list[0], xor_list[4] = xor_list[4], xor_list[0]
    if relay[1] == '0':
        xor_list[4], xor_list[3] = xor_list[3], xor_list[4]
    if relay[2] == '0':
        xor_list[3], xor_list[2] = xor_list[2], xor_list[3]
    if relay[3] == '0':
        xor_list[2], xor_list[1] = xor_list[1], xor_list[2]
    if relay[4] == '0':
        xor_list[1], xor_list[0] = xor_list[0], xor_list[1]

    return "".join(xor_list)




# generating 5 bit sequences
bitgroups5 = ["".join(x) for x in
              [list(x) for x in
               list(itertools.product(["1","0"], repeat=5))]]

xor_result   = sys.argv[1]
relay_config = sys.argv[2]
ciphertext   = relay(xor_result, relay_config)

print(f"R({xor_result}, {relay_config}) = {ciphertext}")

print("--------")
print("Other relay configurations which give the same ciphertext:")

others = False

for x in bitgroups5:
    if x != relay_config and relay(xor_result, x) == ciphertext:
        print(f"R({xor_result}, {x}) = {ciphertext}")
        others = True

if not others:
    print("None")


# 5fa326ca59860a63c683cb32e8ecff9d

from ciphertext import ciphertext as ciphertext
from ciphertext import ciphertext_binary as ciphertext_binary
from ciphertext import letter_shift as letter_shift
from typing import List
import itertools
import sys




# The script takes one argument, which is the index of the plaintext
# character you wish to analyze.
if len(sys.argv) != 2:
    raise Exception("Invalid number of arguments (should be 1)")
if not sys.argv[1].isnumeric():
    raise Exception("Argument needs to be numerical")
if not 0 <= int(sys.argv[1]) <= 1549:
    raise Exception("Argument needs to be in between 0 and 1549")

alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
plaintext = alphabet * 60




# generating 5 bit sequences
bitgroups5 = ["".join(x) for x in
              [list(x) for x in
               list(itertools.product(["1","0"], repeat=5))]]




def weight(binary: str) -> int:
    for x in binary:
        if x not in ['0', '1']:
            raise Exception("Weight error: invalid binary string")
    return binary.count('1')




def xor(binary1: str, binary2: str) -> str:
    """XOR two binary strings.

    :param binary1: The first binary string
    :param binary2: The second binary string

    :returns: The XORed value of these two strings
    """
    if len(binary1) != 5 or len(binary2) != 5:
        print("XOR error: invalid input")
        exit()
    for char in binary1:
        if char not in ['0', '1']:
            print("XOR error: invalid input")
            exit()
    for char in binary2:
        if char not in ['0', '1']:
            print("XOR error: invalid input")
            exit()

    return '{0:b}'.format(int(binary1, 2) ^ int(binary2, 2)).zfill(5)




def relay(xor_result: str, relay: str) -> str:
    """Perform relay swaps on an XOR result.

    :param xor_result: The result after a plaintext character has been
        XORed with the binary 5-bit group on the wheels connected to
        the left of the machine
    :param relay: The 5-bit group on the wheels connected to the right
        of the machine

    :returns: A ciphertext character, after the relay swaps have been
              performed
    """
    if len(xor_result) != 5 or len(relay) != 5:
        raise Exception("Invalid input length")
    for char in xor_result:
        if char not in ['0', '1']:
            raise Exception("Invalid input string: not binary")
    for char in relay:
        if char not in ['0', '1']:
            raise Exception("Invalid input string: not binary")

    xor_list = list(xor_result)

    if relay[0] == '0':
        xor_list[0], xor_list[4] = xor_list[4], xor_list[0]
    if relay[1] == '0':
        xor_list[4], xor_list[3] = xor_list[3], xor_list[4]
    if relay[2] == '0':
        xor_list[3], xor_list[2] = xor_list[2], xor_list[3]
    if relay[3] == '0':
        xor_list[2], xor_list[1] = xor_list[1], xor_list[2]
    if relay[4] == '0':
        xor_list[1], xor_list[0] = xor_list[0], xor_list[1]

    return "".join(xor_list)




def assign_weight_0_5():
    """When the ciphertext character is of weight 0 or 5, assign the
    correct bits to each wheel connected to the left of the machine.
    """
    global w73, w67, w71, w61, w65, left5str

    for count, value in enumerate(ciphertext_binary):
        if value != "00000" and value != "11111":
            continue
        if left5str == xor(letter_shift[plaintext[count]], value):
            continue
        rotate(count)
        w73[current_rotation % 73] = int(xor(letter_shift[plaintext[count]], value)[0])
        w67[current_rotation % 67] = int(xor(letter_shift[plaintext[count]], value)[1])
        w71[current_rotation % 71] = int(xor(letter_shift[plaintext[count]], value)[2])
        w61[current_rotation % 61] = int(xor(letter_shift[plaintext[count]], value)[3])
        w65[current_rotation % 65] = int(xor(letter_shift[plaintext[count]], value)[4])




def assign_weight_1_4():
    """When the ciphertext character is of weight 1 or 4, assign the
    correct bits to each wheel connected to the left of the machine.
    """
    global w73, w67, w71, w61, w65, left5str

    for count, value in enumerate(ciphertext_binary):
        if weight(value) != 1 and weight(value) != 4:
            continue
        rotate(count)
        if left5str.count("2") != 1:
            continue
        attempt0 = left5str.replace('2', '0')
        attempt1 = left5str.replace('2', '1')
        index2       = left5str.index('2')
        if weight(xor(attempt0, letter_shift[plaintext[count]])) == weight(value):
            if   index2 == 0: w73[count % 73] = 0
            elif index2 == 1: w67[count % 67] = 0
            elif index2 == 2: w71[count % 71] = 0
            elif index2 == 3: w61[count % 61] = 0
            elif index2 == 4: w65[count % 65] = 0
        elif weight(xor(attempt1, letter_shift[plaintext[count]])) == weight(value):
            if   index2 == 0: w73[count % 73] = 1
            elif index2 == 1: w67[count % 67] = 1
            elif index2 == 2: w71[count % 71] = 1
            elif index2 == 3: w61[count % 61] = 1
            elif index2 == 4: w65[count % 65] = 1
            

        
    
def possible_relays(xor: str, ciphertext: str) -> List[str]:
    """Given an XOR result and a ciphertext, return a list of possible
    relay configurations which will give this ciphertext as output.

    :param xor_result: The XOR result
    :param ciphertext: The ciphertext

    :returns: A list of possible relay configurations which will
              give this ciphertext as output
    """
    return_value = []

    for x in bitgroups5:
        if relay(xor, x) == ciphertext:
            return_value.append(x)

    return return_value
            
            


def decimal(binary: str) -> int:
    return int(binary, 2)
    

        
    
def assign_right_unique():
    """When there is only one valid relay configuration which gives
    the correct ciphertext, assign this configuration to the relay
    wheels at the correct rotation.
    """
    global w53, w59, w64, w69, w47
    for count, value in enumerate(ciphertext_binary):
        rotate(count)
        xor_result = xor(letter_shift[plaintext[count]], left5str)
        if len(possible_relays(xor_result, value)) == 1:
            assign_value = possible_relays(xor_result, value)[0]
            w53[count % 53] = int(assign_value[0])
            w59[count % 59] = int(assign_value[1])
            w64[count % 64] = int(assign_value[2])
            w69[count % 69] = int(assign_value[3])
            w47[count % 47] = int(assign_value[4])




def assign_right_single_unknown():
    """Once the 'assign_right_unique()' function has been run, this
    function will find the remaining unknown bits on the relay wheels.
    """
    global w53, w59, w64, w69, w47, right5str, left5str
    for count, value in enumerate(ciphertext_binary):
        rotate(count)
        xor_result = xor(letter_shift[plaintext[count]], left5str)
        if right5str.count('2') == 1:
            attempt0 = right5str.replace('2', '0')
            attempt1 = right5str.replace('2', '1')
            relay0   = relay(xor_result, attempt0)
            relay1   = relay(xor_result, attempt1)
            if relay0 == relay1:
                continue
            if relay0 == value:
                w53[count % 53] = int(attempt0[0])
                w59[count % 59] = int(attempt0[1])
                w64[count % 64] = int(attempt0[2])
                w69[count % 69] = int(attempt0[3])
                w47[count % 47] = int(attempt0[4])
            elif relay1 == value:
                w53[count % 53] = int(attempt1[0])
                w59[count % 59] = int(attempt1[1])
                w64[count % 64] = int(attempt1[2])
                w69[count % 69] = int(attempt1[3])
                w47[count % 47] = int(attempt1[4])
                


        
    
# left cables
w73 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
w67 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
w71 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
w61 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
w65 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]

# right cables
w53 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
w59 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
w64 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2] 
w69 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2] 
w47 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]




# The current rotation state of the wheels.
current_rotation = 0
left5 = [
    w73[current_rotation % 73], # correct
    w67[current_rotation % 67], # correct
    w71[current_rotation % 71], # correct
    w61[current_rotation % 61], # correct
    w65[current_rotation % 65], # correct
]
right5 = [
    w53[current_rotation % 53],
    w59[current_rotation % 59],
    w64[current_rotation % 64],
    w69[current_rotation % 69],
    w47[current_rotation % 47],
]

left5str  = "".join([str(x) for x in left5])
right5str = "".join([str(x) for x in right5])




def rotate(rotation=current_rotation + 1):
    """Perform a rotation on the wheels of the machine.

    :param rotation: The absolute rotation to set the wheels to.  A
        value of 0 will set the wheels to the state they would have
        been in after 0 rotations, A value of 20 will set the wheels
        to the state they would have been in after 20 rotations, and
        so on.  If called with no argument, the wheels will simply be
        rotated once (by incrementing the current_rotation variable).

    :returns:
    """
    global current_rotation, left5, left5str, right5, right5str
    current_rotation = rotation
    left5 = [
        w73[current_rotation % 73], # correct
        w67[current_rotation % 67], # correct
        w71[current_rotation % 71], # correct
        w61[current_rotation % 61], # correct
        w65[current_rotation % 65], # correct
    ]
    right5 = [
        w53[current_rotation % 53],
        w59[current_rotation % 59],
        w64[current_rotation % 64],
        w69[current_rotation % 69],
        w47[current_rotation % 47],
    ]
    left5str  = "".join([str(x) for x in left5])
    right5str = "".join([str(x) for x in right5])




assign_weight_0_5()
assign_weight_1_4()
assign_right_unique()
assign_right_single_unknown()
#assign_right_single_unknown()





# 1ST PRINT
for count, value in enumerate(ciphertext_binary):
    if count > 100: break

    rotate(count)

    pt     = plaintext[count]
    ptbn   = letter_shift[pt]
    ptwxor = xor(ptbn, left5str)

    print(f"{count}:\t{pt} = {ptbn} ({decimal(ptbn)})\t\tleft wheels: {left5str} ({decimal(left5str)}); \tXOR: {ptwxor} ({decimal(ptwxor)})\t\trelay: {right5str}\tc = {value} ({decimal(value)});")




# 2ND PRINT
#for count, value in enumerate(ciphertext_binary):
#    #if count > 100: break
#
#    rotate(count)
#
#    formatted = f"{count}:\t{value};\t{plaintext[count]} = {letter_shift[plaintext[count]]}\tyour wheels: {left5str}"
#
#    if value == "11111":
#        print(f"full (one)    {formatted}", end="")
#        if xor(letter_shift[plaintext[count]], value) == left5str:
#            print(f"\txor: ", end="")
#            print("\u001b[32m", end="")
#            print(f"{xor(letter_shift[plaintext[count]], value)}", end="")
#            print("\u001b[0m")
#        else:
#            print(f"\txor: ", end="")
#            print(f"{xor(letter_shift[plaintext[count]], value)}")
#
#    elif value == "00000":
#        print(f"full (zero)   {formatted}", end="")
#        if xor(letter_shift[plaintext[count]], value) == left5str:
#            print(f"\txor: ", end="")
#            print("\u001b[32m", end="")
#            print(f"{xor(letter_shift[plaintext[count]], value)}", end="")
#            print("\u001b[0m")
#        else:
#            print(f"\txor: ", end="")
#            print(f"{xor(letter_shift[plaintext[count]], value)}")
#
#    #elif value in ["00001", "00010", "00100", "01000", "10000",
#    #               "11110", "11101", "11011", "10111", "01111"]:
#
#    #    print(f"weight 1 or 4 {formatted}")
#
#    #else:
#    #    print(f"              {formatted}")



# Replacing occurrences of 2 with 0 for compatibility with xor
for x in range(len(w73)):
    if w73[x] == 2: w73[x] = 0
for x in range(len(w67)):
    if w67[x] == 2: w67[x] = 0
for x in range(len(w71)):
    if w71[x] == 2: w71[x] = 0
for x in range(len(w61)):
    if w61[x] == 2: w61[x] = 0
for x in range(len(w65)):
    if w65[x] == 2: w65[x] = 0


print()
test_num         = int(sys.argv[1])
character        = plaintext[test_num]
character_binary = letter_shift[character]




rotate(test_num)





print("Plaintext:")
print(test_num, character_binary, character)
print()

print("Ciphertext:")
print(f"{ciphertext_binary[test_num]} ({decimal(ciphertext_binary[test_num])})")
print()

print("Your wheels:")
print(f"{left5str} ({decimal(left5str)})")
print()

print("Wheel indices:")
print(
    test_num % 73,
    test_num % 67,
    test_num % 71,
    test_num % 61,
    test_num % 65
)
print()

print("XOR result:")
print(f"{xor(character_binary, left5str)} ({decimal(xor(character_binary, left5str))})")
print()

#for current in range(test_num, 1550, 26):
#    character        = plaintext[current]
#    character_binary = letter_shift[character]
#    rotate(current)
#    candidates = 0
#
#    for test_xor in bitgroups5:
#        test_xor_result = xor(test_xor, character_binary)
#        for relay_value in bitgroups5:
#            test_relay  = relay(test_xor_result, relay_value)
#            color_code  = "\u001b[32m" if test_xor == left5str else "\u001b[31m"
#            if test_relay == ciphertext_binary[current] and test_xor == left5str:
#                candidates += 1
#                print(f"{current} {character_binary} XOR {color_code + test_xor}\u001b[0m = {test_xor_result}, {test_xor_result} relayed with {relay_value} = {test_relay}")
#
#    if candidates == 0:
#        print(f"{current} is incorrect ({left5str}), {current % 73} {current % 67} {current % 71} {current % 61} {current % 65}")
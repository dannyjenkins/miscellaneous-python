# Planned features:
# - Reading the contender list from a file

import random


def makeChoice(option1, option2):
    """Take two elements as input and returns whichever one the user
    chooses.

    :param option1: One of the elements.

    :param option2: One of the elements.

    :returns: Whichever one the user chooses.

    """
    answer = input(f"{option1} or {option2}? ").lower()
    while answer != option1.lower() and answer != option2.lower():
        answer = input("Not a valid option. Try again:").lower()
    return answer


def getHalf(list):
    """Take a list as input, moves half of it at random into a new list,
    and returns the other list.

    :param list: The input list.

    :returns: A new list, containing half of the elements, chosen at
    random.

    """
    newList = []
    for x in range(len(list) // 2):
        randomElement = random.choice(list)
        newList.append(randomElement)
        list.remove(randomElement)
    return newList


def round(list):
    """Function that performs a round in the championship."""
    newList = []
    print(f"Round of {len(list)}")
    groupOne = getHalf(list)
    groupTwo = list
    for x in range(len(groupOne)):
        newList.append(makeChoice(groupOne[x-1], groupTwo[x-1]))
    return newList

contenders = [
]

while len(contenders) > 1:
    contenders = round(contenders)
print(f"The winner is {contenders[0]}")

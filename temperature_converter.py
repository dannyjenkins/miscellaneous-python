# Fahrenheit to celsius (and vice versa) converter


def ftoc(fahrenheit):
    """Performs a conversion from Fahrenheit to Celsius.

    :param fahrenheit: The number of degrees in Fahrenheit.

    :returns: The number of the degrees in Celsius.

    """
    return float((fahrenheit - 32) / 1.8)

def ctof(celsius):
    """Performs a conversion from Celsius to Fahrenheit.

    :param celsius: The number of degrees in Celsius.

    :returns: The number of the degrees in Fahrenheit.

    """
    return float(celsius * 1.8 + 32)

choice = input('''\nWhat kind of conversion would you like to do?\n
    1. Fahrenheit to celsius
    2. Celsius to fahrenheit\n\n''')

assert choice in ["1", "2"]

number = float(input("Input a number to convert: "))

if choice   == "1": print(f"{number}F is equal to {ftoc(number)}C.")
elif choice == "2": print(f"{number}C is equal to {ctof(number)}F.")

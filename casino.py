import random
import time

yourbalance = 100

while True:
    print(f"Your balance is currently {yourbalance}.\n")

    choice = input("Please specify color and amount, separated by a space:\n\nRed\nBlack\n\n").lower().split()

    while len(choice) != 2:
        choice = input("That's an invalid answer. Please specify color and amount, separated by a space: ").lower().split()

    while choice[0] not in ["red", "black"]:
        choice[0] = input("That's not a valid color. Please choose between red and black: ")

    while choice[1].isdigit() == False:
        choice[1] = input(f"That's not a valid number. Please specify a valid number: ")

    while int(choice[1]) > yourbalance or int(choice[1]) < 0:
        choice[1] = int(input("That's an invalid amount. Please specify another amount: "))

    result = random.choice(["red", "black"])

    print(f"The ball is rolling...\n")
    time.sleep(4)

    if result == choice[0]:
        print(f"The ball landed on {result}. You won!\n")
        yourbalance += int(choice[1])
    elif result != choice[0]:
        print(f"The ball landed on {result}. You lost!\n")
        yourbalance -= int(choice[1])

    if yourbalance == 0:
        print("You have run out of money! Goodbye.")
        time.sleep(1)
        exit()

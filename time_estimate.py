# A simple script that takes an integer as an argument and calculates
# an estimate of how long it would take for that many blocks to be
# mined on the Bitcoin blockchain.

import sys

if len(sys.argv) == 1:
    print("Error: Please provide an integer argument.")
    exit()

blocksLeft  = int(sys.argv[1])
minutesLeft = blocksLeft * 10
hoursLeft   = 0
daysLeft    = 0
yearsLeft   = 0

while minutesLeft >= 60:
    hoursLeft   += 1
    minutesLeft -= 60

while hoursLeft >= 24:
    daysLeft    += 1
    hoursLeft   -= 24

while daysLeft >= 365:
    yearsLeft   += 1
    daysLeft    -= 365

elements = []
if yearsLeft   != 0: elements.append(str(yearsLeft)   + "y")
if daysLeft    != 0: elements.append(str(daysLeft)    + "d")
if hoursLeft   != 0: elements.append(str(hoursLeft)   + "t")
if minutesLeft != 0: elements.append(str(minutesLeft) + "min")

print(" ".join(elements))
